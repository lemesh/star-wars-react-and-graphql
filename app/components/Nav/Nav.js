import React from 'react';
import { Link } from 'react-router-dom';
import './Nav.css';

const Nav = () => (
  <div>
    <nav className="nav">
      <ul className="nav__list">
        <li className="nav__item">
          <Link className="nav__link" to="/films" />
        </li>
        <li className="nav__item">
          <Link className="nav__link" to="/people" />
        </li>
        <li className="nav__item">
          <Link className="nav__link" to="/planets" />
        </li>
        <li className="nav__item">
          <Link className="nav__link" to="/species" />
        </li>
        <li className="nav__item">
          <Link className="nav__link" to="/starships" />
        </li>
        <li className="nav__item">
          <Link className="nav__link" to="/vehicles" />
        </li>
      </ul>
    </nav>
  </div>
);

export default Nav;
