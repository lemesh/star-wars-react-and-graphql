import React from 'react';
import './Footer.css';

const Footer = () => (
    <div className="footer">
    Copyright © 2020 Oleh Lemeshenko. All rights reserved.
    </div>
);

export default Footer;
