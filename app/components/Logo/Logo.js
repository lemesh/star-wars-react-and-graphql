import React from 'react';
import './Logo.css';
import StarWars from '../../assets/images/starwars.svg';
import { Link } from 'react-router-dom';

const Logo = () => (
    <Link to="/">
        <StarWars className="app__logo" />
    </Link>
);

export default Logo;
