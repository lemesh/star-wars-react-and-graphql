import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

const client = new ApolloClient({
  uri: 'http://localhost:61226/'
});

import Logo from './components/Logo/Logo';
import Nav from './components/Nav/Nav';
import Films from './components/Films/Films';
import Footer from './components/Footer/Footer';

const App = () => (
  <ApolloProvider client={client}>
    <BrowserRouter>
      <Logo />
      <Switch>
        <Route exact path="/" component={Nav} />
        <Route exact path="/films" component={Films} />
      </Switch>
      <Footer />
    </BrowserRouter>
  </ApolloProvider>
);

export default App;
